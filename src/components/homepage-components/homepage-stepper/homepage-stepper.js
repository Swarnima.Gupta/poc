import React, { Component } from "react"
import "./homepage-stepper.css"

import "bootstrap/dist/css/bootstrap.min.css"





export default class HomepageStepper extends Component {
  constructor(props) {
    super(props);
    this.switchImage = this.switchImage.bind(this);
    this.state = {
      currentImage: 0,
      images: [
        "/stepper-image/stepper-image-1.png",
        "/stepper-image/bhupendra.png",
        "/stepper-image/akhil.jpg",

      ]
    };
  }


  switchImage() {
    if (this.state.currentImage < this.state.images.length - 1) {
      this.setState({
        currentImage: this.state.currentImage + 1
      });
    } else {
      this.setState({
        currentImage: 0
      });
    }
    return this.currentImage;
  }

  componentDidMount() {
    // setInterval(this.switchImage, 1000);
  }


  render() {

    const imageArray = [
      {id:1, src:'/stepper-image/stepper-image-1.png', description:'1:1 Personalized Mentoring'},
      {id:2, src:'/stepper-image/bhupendra.png', description:'Practically Proven Curriculum'},
      {id:2, src:'/stepper-image/akhil.jpg', description:'Industry Professionals as Instructors'},
      {id:2, src:'/stepper-image/stepper-image-1.png', description:'Interview & Job Preparation'},
      {id:2, src:'/stepper-image/bhupendra.png', description:'Domain Specialist Doubt Solving'},
      {id:2, src:'/stepper-image/akhil.jpg', description:'Peer to Peer Discussions'},

    ]


    const imageDisplay = imageArray.map((id) => {
      return (

        <div>
          <div className="stepper-row ">
            <div className="step-image stepper-wrapper">
              <img  src={id.src} alt="cleaning images" className="ruby-img" />

            </div>
            <div className="step-description">
               <span>
                 {id.description}
               </span>
            </div>
          </div>

          <div className="divider-line">

          </div>
        </div>





      );
    })



    return <div className="container">
      <div className="row stepper-container ">
        <div className="col-md-6">

            <div >
              {imageDisplay}
            </div>

        </div>

        <div className="col-md-6">
          <img
            src={this.state.images[this.state.currentImage]}
            alt="cleaning images" className="image-container"
          />


        </div>
      </div>


    </div>



  }
}
